Dir[File.join(__dir__, 'model', '*.rb')].each { |file| require file }
# walking skeleton:
# 1. leer input
# 2. delegar al modelo
# 3. formatear ouput

parametros = Parser.new.generar_parametros(ARGV)
resultado = Cifrador.new.cifrar(parametros.desplazamiento, parametros.cadena)
Consola.new.imprimir(parametros, resultado)
