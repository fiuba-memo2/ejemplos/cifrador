class Cifrador
  def cifrar(desplamiento, cadena)
    resultado = ''
    cadena.split('').each do |c|
      valor_ascii_input = c.ord
      valor_ascii_output = valor_ascii_input + desplamiento
      resultado += valor_ascii_output.chr
    end
    resultado
  end
end
