Cifrador
=========

[![pipeline status](https://gitlab.com/fiuba-memo2/ejemplos/cifrador/badges/solucion_modelo_pasivo/pipeline.svg)](https://gitlab.com/fiuba-memo2/ejemplos/cifrador/-/commits/solucion_modelo_pasivo)


Aplicación de línea de comandos que encripta cadenas de carateres 
utilizando código césar.

* input: desplazamiento y cadena
* output: cadena original y cadena cifrada 


Ejemplo  de uso
---------------

```
# ruby cifrador_app.rb <desplazamiento> <cadena>
ruby cifrador_app.rb 3 hola
f(3,hola) = krod
```



Nota: esta aplicació no está completa, lo desarrollado está enfocado en mostrar los conceptos centrales del proceso de desarrollo guiado por pruebas propuesto por Freeman y Pryce.

