require 'spec_helper'

describe 'cifrador_app' do

  it 'cifrado de hola con 8 es pwti' do
    resultado = `ruby cifrador_app.rb 8 hola`
    expect(resultado.strip).to eq 'f(8,hola) = pwti'
  end

  it 'cifrado de aaa con 1 es bbb' do
    resultado = `ruby cifrador_app.rb 1 aaa`
    expect(resultado.strip).to eq 'f(1,aaa) = bbb'
  end

end
