require 'spec_helper'

describe 'Parser' do

  it 'genera parametros con desplazamiento' do
    arg = ['3', 'hola']
    parametros = Parser.new().generar_parametros(arg)
    expect(parametros.desplazamiento).to eq 3
  end

  it 'genera parametros con cadena' do
    arg = ['3', 'hola']
    parametros = Parser.new().generar_parametros(arg)
    expect(parametros.cadena).to eq 'hola'

  end
end
