require 'spec_helper'

describe 'Cifrador' do

  it 'cifra 0,hola como hola' do
    resultado = Cifrador.new.cifrar(0,'hola')
    expect(resultado).to eq 'hola'
  end

  it 'cifra 1,hola como ipmb' do
    resultado = Cifrador.new.cifrar(1,'hola')
    expect(resultado).to eq 'ipmb'
  end

  it 'cifra 2,chola como ejqnc' do
    resultado = Cifrador.new.cifrar(2,'chola')
    expect(resultado).to eq 'ejqnc'
  end

end
